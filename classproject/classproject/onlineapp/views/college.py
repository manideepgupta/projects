from django.views import View
from onlineapp.models import *
from django.urls import resolve
from django.shortcuts import  render,get_object_or_404,redirect
from onlineapp.forms.colleges import *
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
import logging
class CollegeView(LoginRequiredMixin,View):
    login_url = '/login/'

    def get(self,request,*args,**kwargs):

        collges=College.objects.values('name','acronym')
        k=request.user.get_all_permissions()
        logging.error("ERROR")
        return render(
            request,
            template_name='get_all_collges_markslist.html',
            context={
                'colleges':collges,
                'permissions':request.user.get_all_permissions()
            }

        )

class addCollegeView(PermissionRequiredMixin,LoginRequiredMixin,View):
    login_url = '/login/'
    permission_required = ('onlineapp.add_college','onlineapp.change_college','onlineapp.delete_college')
    def get(self,request,*args,**kwargs):
        if resolve(request.path_info).url_name=='delete_college_html':

            c=College.objects.get(acronym=kwargs.get('acronym')).delete()
            return redirect('collegs_html')
        form=addCollege()
        if(kwargs):

            college=College.objects.get(**kwargs)
            form=addCollege(instance=college)
        return render(request,template_name='add_college.html',context={'form':form})
    def post(self,request,*args,**kwargs):
        if resolve(request.path_info).url_name=='edit_college_html':
            permission_required = 'onlineapp.edit_college'
            c=College.objects.get(acronym=kwargs.get('acronym'))
            form=addCollege(request.POST,instance=c)
            if form.is_valid():
                form.save()
            return redirect('collegs_html')

        form = addCollege(request.POST)
        if form.is_valid():
            form.save()
            return redirect('collegs_html')



