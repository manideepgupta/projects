from rest_framework import serializers
from onlineapp.models import College,MockTest1,Student
class CollegeSerializer(serializers.ModelSerializer):
    class Meta:
        model = College
        fields = ('id','name','acronym','location','contact')
class MockTestSerializer(serializers.ModelSerializer):
    class Meta:
        model=MockTest1
        fields=('id','problem1','problem2','problem3','problem4')
class StudentSerializer(serializers.ModelSerializer):
    mocktest1=MockTestSerializer(many=False)
    class Meta:
        model=Student
        fields=('id','name','dob','email','db_folder','dropped_out','mocktest1')
    def create(self,validated_data):
        mocktest1=validated_data.pop('mocktest1')
        validated_data['college']=College.objects.get(id=self.context['pk'])
        student=Student.objects.create(**validated_data)

        MockTest1.objects.create(student=student,**mocktest1)
        return student
    def update(self,instance,validated_data):
        mocktest1 = validated_data.pop('mocktest1')
        instance.name=validated_data.get('name',instance.name)
        instance.dob = validated_data.get('dob', instance.dob)
        instance.email = validated_data.get('email', instance.email)
        instance.db_folder = validated_data.get('db_folder', instance.db_folder)
        instance.dropped_out = validated_data.get('dropped_out', instance.dropped_out)
        instance.mocktest1.problem1 = mocktest1['problem1']
        instance.mocktest1.problem2 = mocktest1['problem2']
        instance.mocktest1.problem3 = mocktest1['problem3']
        instance.mocktest1.problem4 = mocktest1['problem4']
        instance.mocktest1.total=mocktest1['problem1']+mocktest1['problem2']+mocktest1['problem3']+mocktest1['problem4']
        instance.mocktest1.save()
        instance.save()
        return instance