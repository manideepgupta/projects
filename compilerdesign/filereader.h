#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#define DISK_SIZE (100*1024*1024)
#define BLOCK_SIZE (16*1024)
#define BLOCKS(size) ((size/BLOCK_SIZE)+(!!(size%BLOCK_SIZE)))
#define START_FILE_OFFSET (2*BLOCK_SIZE+1)
#define ARRAY_OFFSET (BLOCK_SIZE+1)
#define META_OFFSET (12+1)
#define CHECKER (0x444E524D)
#define FILES (32)

struct fileinfo
{
	char name[20];
	int length;
	int blockno;
	int block_count;

};
struct metaData
{
	unsigned int magicNumber;
	int no_of_files;
	int free_blocks;
	struct fileinfo fileDetails[FILES];
	char bitVector[FILES * 5];
};
int blockRead(int blockNo, void *buffer);
int blockWrite(int blockNo, void *buffer);
void format();
int fileSize(char *filename);
int findStartBlockNo(char bitvector[]);
void addFileDetails(char *filename, struct metaData metadata, int blocks, int blockno, int length, int list[]);
int copy_to_disk(char *source, char *destination);
int copy_from_disk(char *source, char *destination);
int deleteFile(char *filename);
void ls();
void debug();
int isEqual(char *str1, char *str2);
void print(void *buffer, int blocksize);
void readfromfile(char *filename);
