#include "utilities.h"
int strtoint(char *str)
{
	int i = 0;
	int sum = 0;
	while (str[i] != '\0')
	{
		sum *= 10;
		sum += (str[i] - '0');
		i++;
	}
	return sum;
}
int allocate_memoryV(char * variable, int tableno, struct variable table[], int address, int size)
{
	strcpy(table[tableno].name, variable);
	table[tableno].address = address;
	table[tableno].size = size;

	return tableno + 1;
}

int allocate_memoryL(char * label, int tableno, struct block table[], int lineno)
{
	strcpy(table[tableno].name, label);
	table[tableno].lineno = lineno;

	return tableno + 1;
}
int getOpcode(char *pneumonics, int flag)
{
	enum opcode Opcode;

	if (strcmp(pneumonics, "MOV") == 0)
	{
		if (flag)
			return 2;
		return 1;
	}
	if (strcmp(pneumonics, "ADD") == 0)
	{
		return 3;
	}
	if (strcmp(pneumonics, "SUB") == 0)
	{
		return 4;
	}
	if (strcmp(pneumonics, "MUL") == 0)
	{
		return 5;
	}
	if (strcmp(pneumonics, "JMP") == 0)
	{
		return 6;
	}
	if (strcmp(pneumonics, "IF") == 0)
	{
		return 7;
	}
	if (strcmp(pneumonics, "EQ") == 0)
	{
		return 8;
	}
	if (strcmp(pneumonics, "LT") == 0)
	{
		return 9;
	}
	if (strcmp(pneumonics, "GT") == 0)
	{
		return 10;
	}
	if (strcmp(pneumonics, "LTEQ") == 0)
	{
		return 11;

	}
	if (strcmp(pneumonics, "GTEQ") == 0)
	{
		return 12;
	}
	if (strcmp(pneumonics, "PRINT") == 0)
	{
		return 13;
	}
	if (strcmp(pneumonics, "READ") == 0)
	{
		return 14;
	}


	return -1;
}
int getRegisterCode(char *registername)
{
	if (strcmp(registername, "AX") == 0)
	{
		return 0;
	}
	if (strcmp(registername, "BX") == 0)
	{
		return 1;
	}
	if (strcmp(registername, "CX") == 0)
	{
		return 2;
	}
	if (strcmp(registername, "DX") == 0)
	{
		return 3;
	}
	if (strcmp(registername, "EX") == 0)
	{
		return 4;
	}
	if (strcmp(registername, "FX") == 0)
	{
		return 5;
	}
	if (strcmp(registername, "GX") == 0)
	{
		return 0;
	}
	if (strcmp(registername, "HX") == 0)
	{
		return 0;
	}
	return -1;

}
int getVariable(char *name, struct variable table[])
{
	for (int i = 0; i < 26; i++)
	{
		if (strcmp(name, table[i].name) == 0)
		{
			return table[i].address;
		}
	}
	return -1;
}

int getlineNo(char *name, struct block table[])
{
	for (int i = 0; i < 26; i++)
	{
		if (strcmp(name, table[i].name) == 0)
		{
			return table[i].lineno;
		}
	}
	return -1;

}
char *removecommas(char *record)
{
	char *r = (char*)malloc(sizeof(char) * 10);
	int i = 0;
	if (record == NULL)
		return NULL;
	while (record[i] != ',' && record[i] != '\0'&&record[i] != '\n')
	{
		r[i] = record[i];
		i++;
	}
	r[i] = '\0';
	return r;
}
int isVariable(char *c)
{
	int i = 0;
	while (c[i] != '\0')
	{
		if (c[i] == '[')
			return 0;
		i++;
	}
	return 1;
}
int getSize(char *arrayVariable)
{
	int i = 0;
	int sum = 0;
	while (arrayVariable[i] != '\0'&&arrayVariable[i] != '[')
	{

		i++;
	}
	if (arrayVariable[i] == '\0')
		return 0;
	i++;
	while (arrayVariable[i] != ']')
	{
		sum = sum * 10;
		int k = arrayVariable[i];
		sum += (k - '0');
		i++;
	}
	return sum;

}
char *getVariblefromArray(char *record)
{
	int i = 0;
	char *r = (char *)malloc(sizeof(char) * 10);
	while (record[i] != '['&&record[i] != '\0')
	{
		r[i] = record[i];
		i++;
	}
	r[i] = '\0';
	return r;
}
int isLabel(char *label)
{
	int i = 0;
	while (label[i] != '\0')
	{
		if (label[i] == ':')
			return 1;
		i++;
	}
	return 0;
}
char *removeTabSpaces(char *record)
{
	char *r = (char *)malloc(sizeof(char) * 100);
	int i = 0;
	while (record[i] == '\t')
	{
		i++;
	}
	int k = 0;
	while (record[i] != '\0')
	{
		r[k++] = record[i];
		i++;
	}
	r[k] = '\0';
	return r;
}
char *removecolon(char *label)
{
	int i = 0;
	char *r = (char *)malloc(sizeof(char) * 100);
	int k = 0;
	while (label[i] != '\0'&&label[i] != ':')
	{
		r[k++] = label[i];
		i++;
	}
	r[k] = '\0';
	return r;

}