# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 06:21:19 2019

@author: A
"""
#importing libraries
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#importing datasets
dataset = pd.read_csv('classified_dataset.csv')
dataset['ca']=pd.to_numeric(dataset['ca'],errors='coerce')
dataset['thal']=pd.to_numeric(dataset['thal'],errors='coerce')
X = dataset.iloc[:,:-1].values
y = dataset.iloc[:,-1].values
#taking care of missing values
from sklearn.preprocessing import Imputer
imputer = Imputer(missing_values = 'NaN', strategy='mean' ,axis = 0)
imputer.fit(X[:,:])
X[:,:]=imputer.transform(X[:,:])

#Encoding Categorical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
onehotencoder = OneHotEncoder(categorical_features=[2,12])
X = onehotencoder.fit_transform(X).toarray()
#onehotencoder = OneHotEncoder(categorical_features=[0])
#y=y.reshape(len(y),1)
#y=onehotencoder.fit_transform(y).toarray()

#splitting the dataset into test_train data
from sklearn.model_selection import train_test_split 
x_train,x_test,y_train,y_test = train_test_split(X,y,test_size=0.2,random_state=0)


#feature scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
x_train  = sc.fit_transform(x_train)
x_test= sc.transform(x_test)

#Decisiontree model building
from sklearn.tree import DecisionTreeClassifier
classifier = DecisionTreeClassifier(criterion='entropy',random_state=0)
classifier.fit(x_train,y_train)

#predicting the test set results
y_pred= classifier.predict(x_test)


#building confusion matrix
from sklearn.metrics import confusion_matrix,accuracy_score
cm=confusion_matrix(y_test,y_pred)
v= accuracy_score(y_test,y_pred)


#Ann model building
#import keras
#from keras.models import Sequential
#from keras.layers import Dense
#classifier  = Sequential()
#classifier.add(Dense(activation='relu',input_dim=19,units=36,kernel_initializer='uniform'))
#classifier.add(Dense(activation='relu',units=25,kernel_initializer='uniform'))
#classifier.add(Dense(activation='softmax',units=5,kernel_initializer='uniform'))
#classifier.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])
#classifier.fit(x_train,y_train,batch_size=10,epochs=200)
#y_p = classifier.predict(x_test)
#y_p=(y_p>0.9)
#y_test=onehotencoder.inverse_transform(y_test)
#y_p=onehotencoder.inverse_transform(y_p)


#from sklearn.metrics import confusion_matrix,accuracy_score
#
#cm=confusion_matrix(y_test,y_p)
#v= accuracy_score(y_test,y_p)
