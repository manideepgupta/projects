from openpyxl import load_workbook
from bs4 import BeautifulSoup
import csv
import os
import django
import click
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ipl.settings')
django.setup()

from iplcloneapp.models import *
filename = 'deliveries.csv'
with open(filename, 'rt') as fp:
    # You can also put the relative path of csv file
    # with respect to the manage.py file
    data=csv.DictReader(fp)
    for row in data:
        d=Delivery(**row)
        d.save()

