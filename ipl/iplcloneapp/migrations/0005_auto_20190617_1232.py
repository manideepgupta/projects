# Generated by Django 2.2.1 on 2019-06-17 07:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iplcloneapp', '0004_auto_20190617_1150'),
    ]

    operations = [
        migrations.AlterField(
            model_name='delivery',
            name='batsman',
            field=models.CharField(max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='batting_team',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='bowler',
            field=models.CharField(max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='bowling_team',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='dismissal_kind',
            field=models.CharField(max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='fielder',
            field=models.CharField(max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='non_striker',
            field=models.CharField(max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='player_dismissed',
            field=models.CharField(max_length=30, null=True),
        ),
    ]
